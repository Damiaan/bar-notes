//
//  DrankjeMetAantal.swift
//  Bar Notes
//
//  Created by Damiaan on 21/06/2020.
//  Copyright © 2020 Devian. All rights reserved.
//

import SwiftUI

struct DrankjeMetAantalView: View {
	let info: DrankjeMetAantal
	let bestellingen: Bestellingen

	var body: some View {
		HStack {
			Button(drankje.naam) {
				self.bestellingen.voegToe(self.drankje)
			}
			Spacer()

			Text("\( aantal )")
				.frame(minWidth: 50)
				.background(lichtGrijs)
				.cornerRadius(4)

			wegwerpKnop.foregroundColor(aantal == 0 ? lichtGrijs : .accentColor)
		}
	}

	var wegwerpKnop: some View {
		Button(
			action: { self.bestellingen.verwijder(self.drankje) },
			label: { Image(systemName: "minus.circle.fill") }
		)
		.buttonStyle(BorderlessButtonStyle())
	}

	var drankje: Drankje { info.drankje }
	var aantal: Int { info.aantal }
}


struct DrankjeMetAantal_Previews: PreviewProvider {
    static var previews: some View {
		let bestellingen = Bestellingen()
		return DrankjeMetAantalView(
			info: bestellingen.alleAantallen.first!,
			bestellingen: bestellingen
		)
			.padding()
			.previewLayout(PreviewLayout.sizeThatFits)
    }
}
