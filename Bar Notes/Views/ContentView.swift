//
//  ContentView.swift
//  Bar Notes
//
//  Created by Damiaan on 21/06/2020.
//  Copyright © 2020 Devian. All rights reserved.
//

import SwiftUI

struct ContentView: View {
	@ObservedObject var bestellingen = Bestellingen()

    var body: some View {
		NavigationView {
			List {
				ForEach(bestellingen.alleAantallen) { drankEnAantal in
					DrankjeMetAantalView(info: drankEnAantal, bestellingen: self.bestellingen)
				}
			}
			.navigationBarTitle("Bestellingen")
			.navigationBarItems(trailing: titelKnoppen)
		}
    }

	var titelKnoppen: some View {
		HStack(spacing: 20) {
			cocktailKnop
			wegWerpKnop
		}
	}

	var cocktailKnop: some View {
		Button(action: bestellingen.verwijderAlles) {
			Image(systemName: "hourglass")
		}
	}

	var wegWerpKnop: some View {
		Button(action: bestellingen.verwijderAlles) {
			Image(systemName: "bin.xmark")
		}.disabled(bestellingen.totaalAantalDrankjes == 0)
	}
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
		ContentView()
    }
}
