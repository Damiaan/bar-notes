//
//  Notes.swift
//  Bar Notes
//
//  Created by Damiaan on 21/06/2020.
//  Copyright © 2020 Devian. All rights reserved.
//

import Foundation

struct Drankje: ExpressibleByStringLiteral, Hashable, Identifiable {
	let naam: String

	init(stringLiteral: String) {
		naam = stringLiteral
	}

	var id: String { naam }
}

struct DrankjeMetAantal: Identifiable, Hashable {
	let drankje: Drankje
	let aantal: Int

	var id: Drankje { drankje }
}

class Bestellingen: ObservableObject {
	static let alleDrankjes: [Drankje] = ["Fruitsap", "Cola", "Wijn", "Cognac"]

	@Published var aantalDrankjes = [Drankje: Int]()

	var alleAantallen: [DrankjeMetAantal] {
		Bestellingen.alleDrankjes.map { drankje in
			DrankjeMetAantal(drankje: drankje, aantal: aantalDrankjes[drankje] ?? 0)
		}
	}

	func voegToe(_ drankje: Drankje) {
		if aantalDrankjes.keys.contains(drankje) {
			aantalDrankjes[drankje]! += 1
		} else {
			aantalDrankjes[drankje] = 1
		}
	}

	func verwijder(_ drankje: Drankje) {
		if let aantal = aantalDrankjes[drankje] {
			if aantal > 0 {
				aantalDrankjes[drankje]! -= 1
			}
		}
	}

	func verwijderAlles() {
		aantalDrankjes.removeAll()
	}

	var totaalAantalDrankjes: Int {
		aantalDrankjes.values.reduce(0, +)
	}
}
